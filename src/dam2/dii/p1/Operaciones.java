package dam2.dii.p1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Operaciones {
	
	public static void recogerDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//recogemos los datos que introduce el usuario para poder utilizarlos
		String valor_user = request.getParameter("user");
		String valor_pass = request.getParameter("pass");
		String valor_pass2 = request.getParameter("pass2");
		
		//Comprobamos si las contrase�as coinciden
		if(valor_pass.equals(valor_pass2)) {
			//Si coinciden devolvemos un saludo y el nombre del usuario redireccionandolo al index
			request.setAttribute("frase", "Hola " + valor_user);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}else {
			//Si no coinciden le pedimos al usuario que introduzca de nuevo los datos
			request.setAttribute("frase", "Las contrase�as no coinciden, por favor intentelo de nuevo");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		
	}

}
